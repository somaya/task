<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Fav;
use App\Models\Message;
use App\Models\Product;
use App\Models\Rate;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class UsersController extends Controller
{
    public function profile()
    {
        $user=Auth::user();
        return view('website.user.profile',compact('user'));

    }



}
