<?php

namespace App\Http\Controllers\admin;


use App\Models\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminsController extends Controller
{
    public function index()
    {

        $admins = User::where('role',1)->get();
        return view('admin.admins.index', compact('admins'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.admins.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',

        ]);
        $user = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 1,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/admins')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Admin Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.admins.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.admins.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',


        ]);

        $user = User::where('id', $id)->first();
        $user->update([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
        ]);
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $user->update([
                'email' => $request->email
            ]);

        }

        if ($request->password != '') {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/admins' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'Admin Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();

        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'Admin Deleted Successfully']));
    }


}
