<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">Title : </label>
    <div class="col-lg-10{{ $errors->has('title') ? ' has-danger' : '' }}">
        {!! Form::text('title',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"Title"]) !!}
        @if ($errors->has('title'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>

</div>



<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">Subject  : </label>
    <div class="col-lg-10{{ $errors->has('body') ? ' has-danger' : '' }}">
        {!! Form::textarea('body',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>"Subject"]) !!}
        @if ($errors->has('body'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('body') }}</strong>
            </span>
        @endif
    </div>

</div>



<div class="form-group m-form__group row">
    <label for="main_category" class="col-lg-2 col-form-label">Category : </label>
    <div class="col-lg-10{{ $errors->has('category_id') ? ' has-danger' : '' }}">
            <select name="category_id"   class="form-control m-input" required>
                <option value="">Category </option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" {{isset($post) && $post->category->id==$category->id ?'selected':''}}>{{ $category->name }}</option>
                @endforeach
            </select>
        @if ($errors->has('category_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">Image: </label>
    <div class="col-lg-10{{ $errors->has('image') ? ' has-danger' : '' }}">

        <input type="file" name="image"   class="form-control uploadinput">
        @if ($errors->has('image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($post) && $post->image)
    <div class="row">
        {{--@foreach($product->ProductImage as $photo)--}}
        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide" alt="First slide [800x4a00]" src="{{$post->image}}" style="height: 150px; width: 150px" data-holder-rendered="true">
        </div>
        {{--@endforeach--}}

    </div>
@endif







