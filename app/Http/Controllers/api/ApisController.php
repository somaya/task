<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApisController extends Controller
{
    public function getCategories(){
        $categories = Category::all();
        return response()->json($categories, 200);
    }
    public function getPosts($id){
        $posts = Post::where('category_id',$id)->get();
        return response()->json($posts, 200);
    }
    public function updatePostViewNumber(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $post = Post::find($request->post_id);
        $post->update([
            'view_number' => $post->view_number +1
        ]);
        return response()->json([], 204);
    }

}
