<?php

namespace App\Http\Controllers\admin;


use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){

        $users=User::where('role','<>',1)->count();
        $admins=User::where('role',1)->count();
        $categories=Category::count();
        $posts=Post::count();


        return view('admin.index',compact('users','posts','admins','categories'));
    }
}
