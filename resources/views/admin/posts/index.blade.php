@extends('admin.layouts.app')
@section('title')
    Posts
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/posts')}}" class="m-menu__link">
            <span class="m-menu__link-text">Posts</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Posts
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <br>
            <div><a href="{{route('posts.create')}}" style="margin-bottom:20px"
                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i>Add Post</a></div>
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    {{--{{$post}}--}}

                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{$post->title}} </td>
                        <td>{{$post->category?$post->category->name:''}} </td>
                        <td>
                            @if($post->image)
                            <img src="{{$post->image }}" width="120px" height="120px"></td>
                        @endif
                        <td>

                            <a  title="Show" href="/webadmin/posts/{{$post->id}}" ><i class="fa fa-eye"></i></a>
                            <a title="Edit" href="/webadmin/posts/{{$post->id}}/edit"><i class="fa fa-edit"></i></a>
                            <form class="inline-form-style" action="/webadmin/posts/{{ $post->id }}" method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>
@endsection

@section('footer')


@endsection
