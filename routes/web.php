<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;


Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'website\HomeController@home');
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'website\UsersController@profile');
    Route::get('/posts', 'website\HomeController@getPosts');
    Route::get('/posts/{id}', 'website\PostsController@getPost');


});



Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/users', 'admin\UsersController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/categories', 'admin\CategoriesController');
    Route::resource('/posts', 'admin\PostsController');
    Route::get('/get-product-category-data/{product_id}/{category_id}', 'admin\ProductsController@ProductData');
    Route::resource('/sliders', 'admin\SlidersController');
    Route::resource('/user/{id}/rates', 'admin\RatesController');
    Route::get('/send-email', 'admin\UsersController@sendEmail');
    Route::post('/store-email', 'admin\UsersController@storeEmail');



});
