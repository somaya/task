<div class="overlay-citys"></div>
        <link rel="stylesheet" href="{{asset('/website/css/nav.css')}}">
<style>

</style>
<div class="wrap">

    <div class="upper-bar">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-4 align-self-center">
                    <a href="/posts" class="logo-link"><img src="/website/img/logo-h.png" width="120px"/></a>


                </div>
                <div class="col-md-8 align-self-center hide-in-sm" dir="rtl" style="text-align: right">

                    @if(!auth()->check())
                        <a href="/login" class="login-header"><span>Login</span> </a>
                    @else
                        <div style="display: inline-flex; text-align: right" >
                            <span data-toggle="dropdown">
                                    <h4>{{auth()->user()->f_name}} {{auth()->user()->l_name}}  <i class="fa fa-caret-down" aria-hidden="true"></i></h4>
                            </span>
                            <ul class="dropdown-menu" style="right: unset">
                                <li><a href="/profile" data-original-title="" title=""> Profile<i class="fa fa-user" aria-hidden="true"></i> </a></li>

                                @if(auth()->user()->role == 1)
                                    <li><a href="/webadmin/dashboard" data-original-title="" title="">Admin Panel <i class="fa fa-users" aria-hidden="true"></i> </a></li>
                                @endif
                                <li><a href="/logout" data-original-title="" title=""> Logout <i class="fa fa-power-off" aria-hidden="true"></i> </a></li>
                            </ul>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>


    <header id="header" class="hide-in-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </header>
</div>
<script>
    function menueUpdate(id){
        $('.children_dropdown').hide(200);
        $('#menu-'+id).show(200);
    }
</script>
