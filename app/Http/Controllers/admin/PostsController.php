<?php

namespace App\Http\Controllers\admin;


use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use function MongoDB\BSON\fromJSON;
use function MongoDB\BSON\toJSON;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $posts  = Post::all();
        return view('admin.posts.index', compact('posts'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.posts.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required|string',
            'category_id' => 'required',
        ]);

        $post = Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'category_id' => $request->category_id,

        ]);
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('image')->extension();
            $request->file('image')->move(
                base_path() . '/public/uploads/posts/', $imageName
            );
            $post->image = '/uploads/posts/' . $imageName;
            $post->save();

        }

        return redirect('/webadmin/posts')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Post Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('id', $id)->first();
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('id', $id)->first();
        $categories = Category::all();
        return view('admin.posts.edit', compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required|string',
            'category_id' => 'required',

        ]);

        $images = request()->file('images');
        $images_pathes = [];

        if (count($images ??[])){
            foreach ($images as $key => $image) {
                $imageName = Str::random(10) . '.' . $image->extension();
                $image->move(
                    base_path() . '/public/uploads/posts/', $imageName
                );
                $images_pathes[$key] = '/uploads/posts/' . $imageName;
            }
        }


        $data = [
            'title' => $request->title,
            'body' => $request->body,
            'category_id' => $request->category_id,
        ];
        $post = Post::where('id', $id)->update($data);

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('image')->extension();
            $request->file('image')->move(
                base_path() . '/public/uploads/posts/', $imageName
            );
            $post->image = '/uploads/posts/' . $imageName;
            $post->save();

        }
        return redirect('/webadmin/posts' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'Post Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::where('id', $id)->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'Post Deleted Successfully']));
    }


}
